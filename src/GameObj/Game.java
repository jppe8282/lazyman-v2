
package GameObj;


public class Game extends GameStream implements IGame {
    private String home, away, time, gameState, timeRemaining, awayFull, homeFull;
    int id;

    @Override
    public String getHomeTeam() {
        return home;
    }

    @Override
    public void setHomeTeam(String team) {
        this.home = team;
    }

    @Override
    public String getAwayTeam() {
        return away;
    }

    @Override
    public void setAwayTeam(String team) {
        this.away = team;
    }

    @Override
    public String getTime() {
        return time;
    }

    @Override
    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String getGameState() {
        return gameState;
    }

    @Override
    public void setGameState(String state) {
        this.gameState = state;
    }

    @Override
    public String getTimeRemaining() {
        return timeRemaining;
    }

    @Override
    public void setTimeRemaining(String timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    @Override
    public String getAwayTeamFull() {
        return awayFull;
    }

    @Override
    public String getHomeTeamFull() {
        return homeFull;
    }

    @Override
    public void setHomeTeamFull(String team) {
        this.homeFull = team;
    }

    @Override
    public void setAwayTeamFull(String team) {
        this.awayFull = team;
    }

    @Override
    public void setGameID(int id) {
        this.id = id;
    }

    @Override
    public int getGameID() {
        return id;
    }
        
}
