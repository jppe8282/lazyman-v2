
package GameObj;


public class GameStream implements IGameStream {

    private String aMID, hMID, nMID, fMID, aTV, hTV, nTV, fTV;

    @Override
    public void setAwayMediaID(String id) {
        aMID=id;
    }

    @Override
    public String getAwayMediaID() {
        return aMID;
    }

    @Override
    public void setHomeMediaID(String id) {
        hMID=id;
    }

    @Override
    public String getHomeMediaID() {
        return hMID;
    }

    @Override
    public void setNationalMediaID(String id) {
        nMID = id;
    }

    @Override
    public String getNationalMediaID() {
        return nMID;
    }

    @Override
    public void setFrenchMediaID(String id) {
        fMID = id;
    }

    @Override
    public String getFrenchMediaID() {
        return fMID;
    }

    @Override
    public void setAwayTVStation(String station) {
        aTV=station;
    }

    @Override
    public String getAwayTVStation() {
        return aTV;
    }

    @Override
    public void setHomeTVStation(String station) {
        hTV=station;
    }

    @Override
    public String getHomeTVStation() {
        return hTV;
    }

    @Override
    public void setNationalTVStation(String station) {
        nTV=station;
    }

    @Override
    public String getNationalTVStation() {
        return nTV;
    }

    @Override
    public void setFrenchTVStation(String station) {
        fTV=station;
    }

    @Override
    public String getFrenchTVStation() {
        return fTV;
    }
    
   
    
}
